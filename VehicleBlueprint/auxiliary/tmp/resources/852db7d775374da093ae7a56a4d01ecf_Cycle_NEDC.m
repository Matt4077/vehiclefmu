%===============================================================================
% Description   : IGNITE - NEDC Drive cycle: time vs. speed (1180sec)
% Filename      : IPowertrain/Resources/DriveCycles/Cycle_NEDC.m
% Creation Date : 20 Jun 2013
%===============: Copyright (c) 2013 Ricardo Software ==========================

% Duration: 1180 sec
% Distance: 6.84 mi / 11.007 km
% Max Speed: 74.56 mph / 120.0 km/hr
% Avg Speed: 20.87 mph / 33.6 km/hr
          
% --- Drive cycle
dc_speed_profile = [
         0.0,     0.00;
         1.0,     0.00;
         2.0,     0.00;
         3.0,     0.00;
         4.0,     0.00;
         5.0,     0.00;
         6.0,     0.00;
         7.0,     0.00;
         8.0,     0.00;
         9.0,     0.00;
        10.0,     0.00;
        11.0,     0.00;
        12.0,     3.80;
        13.0,     7.50;
        14.0,    11.30;
        15.0,    15.00;
        16.0,    15.00;
        17.0,    15.00;
        18.0,    15.00;
        19.0,    15.00;
        20.0,    15.00;
        21.0,    15.00;
        22.0,    15.00;
        23.0,    15.00;
        24.0,    12.00;
        25.0,     9.00;
        26.0,     6.00;
        27.0,     3.00;
        28.0,     0.00;
        29.0,     0.00;
        30.0,     0.00;
        31.0,     0.00;
        32.0,     0.00;
        33.0,     0.00;
        34.0,     0.00;
        35.0,     0.00;
        36.0,     0.00;
        37.0,     0.00;
        38.0,     0.00;
        39.0,     0.00;
        40.0,     0.00;
        41.0,     0.00;
        42.0,     0.00;
        43.0,     0.00;
        44.0,     0.00;
        45.0,     0.00;
        46.0,     0.00;
        47.0,     0.00;
        48.0,     0.00;
        49.0,     0.00;
        50.0,     2.70;
        51.0,     5.30;
        52.0,     8.00;
        53.0,    10.70;
        54.0,    13.30;
        55.0,    16.00;
        56.0,    18.70;
        57.0,    21.30;
        58.0,    24.00;
        59.0,    26.70;
        60.0,    29.30;
        61.0,    32.00;
        62.0,    32.00;
        63.0,    32.00;
        64.0,    32.00;
        65.0,    32.00;
        66.0,    32.00;
        67.0,    32.00;
        68.0,    32.00;
        69.0,    32.00;
        70.0,    32.00;
        71.0,    32.00;
        72.0,    32.00;
        73.0,    32.00;
        74.0,    32.00;
        75.0,    32.00;
        76.0,    32.00;
        77.0,    32.00;
        78.0,    32.00;
        79.0,    32.00;
        80.0,    32.00;
        81.0,    32.00;
        82.0,    32.00;
        83.0,    32.00;
        84.0,    32.00;
        85.0,    32.00;
        86.0,    29.10;
        87.0,    26.20;
        88.0,    23.30;
        89.0,    20.40;
        90.0,    17.50;
        91.0,    14.50;
        92.0,    11.60;
        93.0,     8.70;
        94.0,     5.80;
        95.0,     2.90;
        96.0,     0.00;
        97.0,     0.00;
        98.0,     0.00;
        99.0,     0.00;
       100.0,     0.00;
       101.0,     0.00;
       102.0,     0.00;
       103.0,     0.00;
       104.0,     0.00;
       105.0,     0.00;
       106.0,     0.00;
       107.0,     0.00;
       108.0,     0.00;
       109.0,     0.00;
       110.0,     0.00;
       111.0,     0.00;
       112.0,     0.00;
       113.0,     0.00;
       114.0,     0.00;
       115.0,     0.00;
       116.0,     0.00;
       117.0,     0.00;
       118.0,     1.90;
       119.0,     3.80;
       120.0,     5.80;
       121.0,     7.70;
       122.0,     9.60;
       123.0,    11.50;
       124.0,    13.50;
       125.0,    15.40;
       126.0,    17.30;
       127.0,    19.20;
       128.0,    21.20;
       129.0,    23.40;
       130.0,    25.00;
       131.0,    26.90;
       132.0,    28.80;
       133.0,    30.80;
       134.0,    32.70;
       135.0,    34.60;
       136.0,    36.50;
       137.0,    38.50;
       138.0,    40.40;
       139.0,    42.30;
       140.0,    44.20;
       141.0,    46.20;
       142.0,    48.10;
       143.0,    50.00;
       144.0,    50.00;
       145.0,    50.00;
       146.0,    50.00;
       147.0,    50.00;
       148.0,    50.00;
       149.0,    50.00;
       150.0,    50.00;
       151.0,    50.00;
       152.0,    50.00;
       153.0,    50.00;
       154.0,    50.00;
       155.0,    50.00;
       156.0,    48.10;
       157.0,    46.20;
       158.0,    44.40;
       159.0,    42.50;
       160.0,    40.60;
       161.0,    38.80;
       162.0,    36.90;
       163.0,    35.00;
       164.0,    35.00;
       165.0,    35.00;
       166.0,    35.00;
       167.0,    35.00;
       168.0,    35.00;
       169.0,    35.00;
       170.0,    35.00;
       171.0,    35.00;
       172.0,    35.00;
       173.0,    35.00;
       174.0,    35.00;
       175.0,    35.00;
       176.0,    35.00;
       177.0,    32.10;
       178.0,    29.20;
       179.0,    26.20;
       180.0,    23.30;
       181.0,    20.40;
       182.0,    17.50;
       183.0,    14.60;
       184.0,    11.70;
       185.0,     8.70;
       186.0,     5.80;
       187.0,     2.90;
       188.0,     0.00;
       189.0,     0.00;
       190.0,     0.00;
       191.0,     0.00;
       192.0,     0.00;
       193.0,     0.00;
       194.0,     0.00;
       195.0,     0.00;
       196.0,     0.00;
       197.0,     0.00;
       198.0,     0.00;
       199.0,     0.00;
       200.0,     0.00;
       201.0,     0.00;
       202.0,     0.00;
       203.0,     0.00;
       204.0,     0.00;
       205.0,     0.00;
       206.0,     0.00;
       207.0,     0.00;
       208.0,     3.80;
       209.0,     7.50;
       210.0,    11.30;
       211.0,    15.00;
       212.0,    15.00;
       213.0,    15.00;
       214.0,    15.00;
       215.0,    15.00;
       216.0,    15.00;
       217.0,    15.00;
       218.0,    15.00;
       219.0,    15.00;
       220.0,    12.00;
       221.0,     9.00;
       222.0,     6.00;
       223.0,     3.00;
       224.0,     0.00;
       225.0,     0.00;
       226.0,     0.00;
       227.0,     0.00;
       228.0,     0.00;
       229.0,     0.00;
       230.0,     0.00;
       231.0,     0.00;
       232.0,     0.00;
       233.0,     0.00;
       234.0,     0.00;
       235.0,     0.00;
       236.0,     0.00;
       237.0,     0.00;
       238.0,     0.00;
       239.0,     0.00;
       240.0,     0.00;
       241.0,     0.00;
       242.0,     0.00;
       243.0,     0.00;
       244.0,     0.00;
       245.0,     0.00;
       246.0,     2.70;
       247.0,     5.30;
       248.0,     8.00;
       249.0,    10.70;
       250.0,    13.30;
       251.0,    16.00;
       252.0,    18.70;
       253.0,    21.30;
       254.0,    24.00;
       255.0,    26.70;
       256.0,    29.30;
       257.0,    32.00;
       258.0,    32.00;
       259.0,    32.00;
       260.0,    32.00;
       261.0,    32.00;
       262.0,    32.00;
       263.0,    32.00;
       264.0,    32.00;
       265.0,    32.00;
       266.0,    32.00;
       267.0,    32.00;
       268.0,    32.00;
       269.0,    32.00;
       270.0,    32.00;
       271.0,    32.00;
       272.0,    32.00;
       273.0,    32.00;
       274.0,    32.00;
       275.0,    32.00;
       276.0,    32.00;
       277.0,    32.00;
       278.0,    32.00;
       279.0,    32.00;
       280.0,    32.00;
       281.0,    32.00;
       282.0,    29.10;
       283.0,    26.20;
       284.0,    23.30;
       285.0,    20.40;
       286.0,    17.50;
       287.0,    14.50;
       288.0,    11.60;
       289.0,     8.70;
       290.0,     5.80;
       291.0,     2.90;
       292.0,     0.00;
       293.0,     0.00;
       294.0,     0.00;
       295.0,     0.00;
       296.0,     0.00;
       297.0,     0.00;
       298.0,     0.00;
       299.0,     0.00;
       300.0,     0.00;
       301.0,     0.00;
       302.0,     0.00;
       303.0,     0.00;
       304.0,     0.00;
       305.0,     0.00;
       306.0,     0.00;
       307.0,     0.00;
       308.0,     0.00;
       309.0,     0.00;
       310.0,     0.00;
       311.0,     0.00;
       312.0,     0.00;
       313.0,     0.00;
       314.0,     1.90;
       315.0,     3.80;
       316.0,     5.80;
       317.0,     7.70;
       318.0,     9.60;
       319.0,    11.50;
       320.0,    13.50;
       321.0,    15.40;
       322.0,    17.30;
       323.0,    19.20;
       324.0,    21.20;
       325.0,    23.40;
       326.0,    25.00;
       327.0,    26.90;
       328.0,    28.80;
       329.0,    30.80;
       330.0,    32.70;
       331.0,    34.60;
       332.0,    36.50;
       333.0,    38.50;
       334.0,    40.40;
       335.0,    42.30;
       336.0,    44.20;
       337.0,    46.20;
       338.0,    48.10;
       339.0,    50.00;
       340.0,    50.00;
       341.0,    50.00;
       342.0,    50.00;
       343.0,    50.00;
       344.0,    50.00;
       345.0,    50.00;
       346.0,    50.00;
       347.0,    50.00;
       348.0,    50.00;
       349.0,    50.00;
       350.0,    50.00;
       351.0,    50.00;
       352.0,    48.10;
       353.0,    46.20;
       354.0,    44.40;
       355.0,    42.50;
       356.0,    40.60;
       357.0,    38.80;
       358.0,    36.90;
       359.0,    35.00;
       360.0,    35.00;
       361.0,    35.00;
       362.0,    35.00;
       363.0,    35.00;
       364.0,    35.00;
       365.0,    35.00;
       366.0,    35.00;
       367.0,    35.00;
       368.0,    35.00;
       369.0,    35.00;
       370.0,    35.00;
       371.0,    35.00;
       372.0,    35.00;
       373.0,    32.10;
       374.0,    29.20;
       375.0,    26.20;
       376.0,    23.30;
       377.0,    20.40;
       378.0,    17.50;
       379.0,    14.60;
       380.0,    11.70;
       381.0,     8.70;
       382.0,     5.80;
       383.0,     2.90;
       384.0,     0.00;
       385.0,     0.00;
       386.0,     0.00;
       387.0,     0.00;
       388.0,     0.00;
       389.0,     0.00;
       390.0,     0.00;
       391.0,     0.00;
       392.0,     0.00;
       393.0,     0.00;
       394.0,     0.00;
       395.0,     0.00;
       396.0,     0.00;
       397.0,     0.00;
       398.0,     0.00;
       399.0,     0.00;
       400.0,     0.00;
       401.0,     0.00;
       402.0,     0.00;
       403.0,     0.00;
       404.0,     3.80;
       405.0,     7.50;
       406.0,    11.30;
       407.0,    15.00;
       408.0,    15.00;
       409.0,    15.00;
       410.0,    15.00;
       411.0,    15.00;
       412.0,    15.00;
       413.0,    15.00;
       414.0,    15.00;
       415.0,    15.00;
       416.0,    12.00;
       417.0,     9.00;
       418.0,     6.00;
       419.0,     3.00;
       420.0,     0.00;
       421.0,     0.00;
       422.0,     0.00;
       423.0,     0.00;
       424.0,     0.00;
       425.0,     0.00;
       426.0,     0.00;
       427.0,     0.00;
       428.0,     0.00;
       429.0,     0.00;
       430.0,     0.00;
       431.0,     0.00;
       432.0,     0.00;
       433.0,     0.00;
       434.0,     0.00;
       435.0,     0.00;
       436.0,     0.00;
       437.0,     0.00;
       438.0,     0.00;
       439.0,     0.00;
       440.0,     0.00;
       441.0,     0.00;
       442.0,     2.70;
       443.0,     5.30;
       444.0,     8.00;
       445.0,    10.70;
       446.0,    13.30;
       447.0,    16.00;
       448.0,    18.70;
       449.0,    21.30;
       450.0,    24.00;
       451.0,    26.70;
       452.0,    29.30;
       453.0,    32.00;
       454.0,    32.00;
       455.0,    32.00;
       456.0,    32.00;
       457.0,    32.00;
       458.0,    32.00;
       459.0,    32.00;
       460.0,    32.00;
       461.0,    32.00;
       462.0,    32.00;
       463.0,    32.00;
       464.0,    32.00;
       465.0,    32.00;
       466.0,    32.00;
       467.0,    32.00;
       468.0,    32.00;
       469.0,    32.00;
       470.0,    32.00;
       471.0,    32.00;
       472.0,    32.00;
       473.0,    32.00;
       474.0,    32.00;
       475.0,    32.00;
       476.0,    32.00;
       477.0,    32.00;
       478.0,    29.10;
       479.0,    26.20;
       480.0,    23.30;
       481.0,    20.40;
       482.0,    17.50;
       483.0,    14.50;
       484.0,    11.60;
       485.0,     8.70;
       486.0,     5.80;
       487.0,     2.90;
       488.0,     0.00;
       489.0,     0.00;
       490.0,     0.00;
       491.0,     0.00;
       492.0,     0.00;
       493.0,     0.00;
       494.0,     0.00;
       495.0,     0.00;
       496.0,     0.00;
       497.0,     0.00;
       498.0,     0.00;
       499.0,     0.00;
       500.0,     0.00;
       501.0,     0.00;
       502.0,     0.00;
       503.0,     0.00;
       504.0,     0.00;
       505.0,     0.00;
       506.0,     0.00;
       507.0,     0.00;
       508.0,     0.00;
       509.0,     0.00;
       510.0,     1.90;
       511.0,     3.80;
       512.0,     5.80;
       513.0,     7.70;
       514.0,     9.60;
       515.0,    11.50;
       516.0,    13.50;
       517.0,    15.40;
       518.0,    17.30;
       519.0,    19.20;
       520.0,    21.20;
       521.0,    23.40;
       522.0,    25.00;
       523.0,    26.90;
       524.0,    28.80;
       525.0,    30.80;
       526.0,    32.70;
       527.0,    34.60;
       528.0,    36.50;
       529.0,    38.50;
       530.0,    40.40;
       531.0,    42.30;
       532.0,    44.20;
       533.0,    46.20;
       534.0,    48.10;
       535.0,    50.00;
       536.0,    50.00;
       537.0,    50.00;
       538.0,    50.00;
       539.0,    50.00;
       540.0,    50.00;
       541.0,    50.00;
       542.0,    50.00;
       543.0,    50.00;
       544.0,    50.00;
       545.0,    50.00;
       546.0,    50.00;
       547.0,    50.00;
       548.0,    48.10;
       549.0,    46.20;
       550.0,    44.40;
       551.0,    42.50;
       552.0,    40.60;
       553.0,    38.80;
       554.0,    36.90;
       555.0,    35.00;
       556.0,    35.00;
       557.0,    35.00;
       558.0,    35.00;
       559.0,    35.00;
       560.0,    35.00;
       561.0,    35.00;
       562.0,    35.00;
       563.0,    35.00;
       564.0,    35.00;
       565.0,    35.00;
       566.0,    35.00;
       567.0,    35.00;
       568.0,    35.00;
       569.0,    32.10;
       570.0,    29.20;
       571.0,    26.20;
       572.0,    23.30;
       573.0,    20.40;
       574.0,    17.50;
       575.0,    14.60;
       576.0,    11.70;
       577.0,     8.70;
       578.0,     5.80;
       579.0,     2.90;
       580.0,     0.00;
       581.0,     0.00;
       582.0,     0.00;
       583.0,     0.00;
       584.0,     0.00;
       585.0,     0.00;
       586.0,     0.00;
       587.0,     0.00;
       588.0,     0.00;
       589.0,     0.00;
       590.0,     0.00;
       591.0,     0.00;
       592.0,     0.00;
       593.0,     0.00;
       594.0,     0.00;
       595.0,     0.00;
       596.0,     0.00;
       597.0,     0.00;
       598.0,     0.00;
       599.0,     0.00;
       600.0,     3.80;
       601.0,     7.50;
       602.0,    11.30;
       603.0,    15.00;
       604.0,    15.00;
       605.0,    15.00;
       606.0,    15.00;
       607.0,    15.00;
       608.0,    15.00;
       609.0,    15.00;
       610.0,    15.00;
       611.0,    15.00;
       612.0,    12.00;
       613.0,     9.00;
       614.0,     6.00;
       615.0,     3.00;
       616.0,     0.00;
       617.0,     0.00;
       618.0,     0.00;
       619.0,     0.00;
       620.0,     0.00;
       621.0,     0.00;
       622.0,     0.00;
       623.0,     0.00;
       624.0,     0.00;
       625.0,     0.00;
       626.0,     0.00;
       627.0,     0.00;
       628.0,     0.00;
       629.0,     0.00;
       630.0,     0.00;
       631.0,     0.00;
       632.0,     0.00;
       633.0,     0.00;
       634.0,     0.00;
       635.0,     0.00;
       636.0,     0.00;
       637.0,     0.00;
       638.0,     2.70;
       639.0,     5.30;
       640.0,     8.00;
       641.0,    10.70;
       642.0,    13.30;
       643.0,    16.00;
       644.0,    18.70;
       645.0,    21.30;
       646.0,    24.00;
       647.0,    26.70;
       648.0,    29.30;
       649.0,    32.00;
       650.0,    32.00;
       651.0,    32.00;
       652.0,    32.00;
       653.0,    32.00;
       654.0,    32.00;
       655.0,    32.00;
       656.0,    32.00;
       657.0,    32.00;
       658.0,    32.00;
       659.0,    32.00;
       660.0,    32.00;
       661.0,    32.00;
       662.0,    32.00;
       663.0,    32.00;
       664.0,    32.00;
       665.0,    32.00;
       666.0,    32.00;
       667.0,    32.00;
       668.0,    32.00;
       669.0,    32.00;
       670.0,    32.00;
       671.0,    32.00;
       672.0,    32.00;
       673.0,    32.00;
       674.0,    29.10;
       675.0,    26.20;
       676.0,    23.30;
       677.0,    20.40;
       678.0,    17.50;
       679.0,    14.50;
       680.0,    11.60;
       681.0,     8.70;
       682.0,     5.80;
       683.0,     2.90;
       684.0,     0.00;
       685.0,     0.00;
       686.0,     0.00;
       687.0,     0.00;
       688.0,     0.00;
       689.0,     0.00;
       690.0,     0.00;
       691.0,     0.00;
       692.0,     0.00;
       693.0,     0.00;
       694.0,     0.00;
       695.0,     0.00;
       696.0,     0.00;
       697.0,     0.00;
       698.0,     0.00;
       699.0,     0.00;
       700.0,     0.00;
       701.0,     0.00;
       702.0,     0.00;
       703.0,     0.00;
       704.0,     0.00;
       705.0,     0.00;
       706.0,     1.90;
       707.0,     3.80;
       708.0,     5.80;
       709.0,     7.70;
       710.0,     9.60;
       711.0,    11.50;
       712.0,    13.50;
       713.0,    15.40;
       714.0,    17.30;
       715.0,    19.20;
       716.0,    21.20;
       717.0,    23.40;
       718.0,    25.00;
       719.0,    26.90;
       720.0,    28.80;
       721.0,    30.80;
       722.0,    32.70;
       723.0,    34.60;
       724.0,    36.50;
       725.0,    38.50;
       726.0,    40.40;
       727.0,    42.30;
       728.0,    44.20;
       729.0,    46.20;
       730.0,    48.10;
       731.0,    50.00;
       732.0,    50.00;
       733.0,    50.00;
       734.0,    50.00;
       735.0,    50.00;
       736.0,    50.00;
       737.0,    50.00;
       738.0,    50.00;
       739.0,    50.00;
       740.0,    50.00;
       741.0,    50.00;
       742.0,    50.00;
       743.0,    50.00;
       744.0,    48.10;
       745.0,    46.20;
       746.0,    44.40;
       747.0,    42.50;
       748.0,    40.60;
       749.0,    38.80;
       750.0,    36.90;
       751.0,    35.00;
       752.0,    35.00;
       753.0,    35.00;
       754.0,    35.00;
       755.0,    35.00;
       756.0,    35.00;
       757.0,    35.00;
       758.0,    35.00;
       759.0,    35.00;
       760.0,    35.00;
       761.0,    35.00;
       762.0,    35.00;
       763.0,    35.00;
       764.0,    35.00;
       765.0,    32.10;
       766.0,    29.20;
       767.0,    26.20;
       768.0,    23.30;
       769.0,    20.40;
       770.0,    17.50;
       771.0,    14.60;
       772.0,    11.70;
       773.0,     8.70;
       774.0,     5.80;
       775.0,     2.90;
       776.0,     0.00;
       777.0,     0.00;
       778.0,     0.00;
       779.0,     0.00;
       780.0,     0.00;
       781.0,     0.00;
       782.0,     0.00;
       783.0,     0.00;
       784.0,     0.00;
       785.0,     0.00;
       786.0,     0.00;
       787.0,     0.00;
       788.0,     0.00;
       789.0,     0.00;
       790.0,     0.00;
       791.0,     0.00;
       792.0,     0.00;
       793.0,     0.00;
       794.0,     0.00;
       795.0,     0.00;
       796.0,     0.00;
       797.0,     0.00;
       798.0,     0.00;
       799.0,     0.00;
       800.0,     0.00;
       801.0,     0.00;
       802.0,     0.00;
       803.0,     0.00;
       804.0,     0.00;
       805.0,     3.00;
       806.0,     6.00;
       807.0,     9.00;
       808.0,    12.00;
       809.0,    15.00;
       810.0,    15.00;
       811.0,    15.00;
       812.0,    17.20;
       813.0,    19.40;
       814.0,    21.70;
       815.0,    23.90;
       816.0,    26.10;
       817.0,    28.30;
       818.0,    30.60;
       819.0,    32.80;
       820.0,    35.00;
       821.0,    35.00;
       822.0,    35.00;
       823.0,    36.90;
       824.0,    38.80;
       825.0,    40.60;
       826.0,    42.50;
       827.0,    44.40;
       828.0,    46.30;
       829.0,    48.10;
       830.0,    50.00;
       831.0,    50.00;
       832.0,    50.00;
       833.0,    51.50;
       834.0,    53.10;
       835.0,    54.60;
       836.0,    56.20;
       837.0,    57.70;
       838.0,    59.20;
       839.0,    60.80;
       840.0,    62.30;
       841.0,    63.80;
       842.0,    65.40;
       843.0,    66.90;
       844.0,    68.50;
       845.0,    70.00;
       846.0,    70.00;
       847.0,    70.00;
       848.0,    70.00;
       849.0,    70.00;
       850.0,    70.00;
       851.0,    70.00;
       852.0,    70.00;
       853.0,    70.00;
       854.0,    70.00;
       855.0,    70.00;
       856.0,    70.00;
       857.0,    70.00;
       858.0,    70.00;
       859.0,    70.00;
       860.0,    70.00;
       861.0,    70.00;
       862.0,    70.00;
       863.0,    70.00;
       864.0,    70.00;
       865.0,    70.00;
       866.0,    70.00;
       867.0,    70.00;
       868.0,    70.00;
       869.0,    70.00;
       870.0,    70.00;
       871.0,    70.00;
       872.0,    70.00;
       873.0,    70.00;
       874.0,    70.00;
       875.0,    70.00;
       876.0,    70.00;
       877.0,    70.00;
       878.0,    70.00;
       879.0,    70.00;
       880.0,    70.00;
       881.0,    70.00;
       882.0,    70.00;
       883.0,    70.00;
       884.0,    70.00;
       885.0,    70.00;
       886.0,    70.00;
       887.0,    70.00;
       888.0,    70.00;
       889.0,    70.00;
       890.0,    70.00;
       891.0,    70.00;
       892.0,    70.00;
       893.0,    70.00;
       894.0,    70.00;
       895.0,    70.00;
       896.0,    67.50;
       897.0,    65.00;
       898.0,    62.50;
       899.0,    60.00;
       900.0,    57.50;
       901.0,    55.00;
       902.0,    52.50;
       903.0,    50.00;
       904.0,    50.00;
       905.0,    50.00;
       906.0,    50.00;
       907.0,    50.00;
       908.0,    50.00;
       909.0,    50.00;
       910.0,    50.00;
       911.0,    50.00;
       912.0,    50.00;
       913.0,    50.00;
       914.0,    50.00;
       915.0,    50.00;
       916.0,    50.00;
       917.0,    50.00;
       918.0,    50.00;
       919.0,    50.00;
       920.0,    50.00;
       921.0,    50.00;
       922.0,    50.00;
       923.0,    50.00;
       924.0,    50.00;
       925.0,    50.00;
       926.0,    50.00;
       927.0,    50.00;
       928.0,    50.00;
       929.0,    50.00;
       930.0,    50.00;
       931.0,    50.00;
       932.0,    50.00;
       933.0,    50.00;
       934.0,    50.00;
       935.0,    50.00;
       936.0,    50.00;
       937.0,    50.00;
       938.0,    50.00;
       939.0,    50.00;
       940.0,    50.00;
       941.0,    50.00;
       942.0,    50.00;
       943.0,    50.00;
       944.0,    50.00;
       945.0,    50.00;
       946.0,    50.00;
       947.0,    50.00;
       948.0,    50.00;
       949.0,    50.00;
       950.0,    50.00;
       951.0,    50.00;
       952.0,    50.00;
       953.0,    50.00;
       954.0,    50.00;
       955.0,    50.00;
       956.0,    50.00;
       957.0,    50.00;
       958.0,    50.00;
       959.0,    50.00;
       960.0,    50.00;
       961.0,    50.00;
       962.0,    50.00;
       963.0,    50.00;
       964.0,    50.00;
       965.0,    50.00;
       966.0,    50.00;
       967.0,    50.00;
       968.0,    50.00;
       969.0,    50.00;
       970.0,    50.00;
       971.0,    50.00;
       972.0,    50.00;
       973.0,    51.50;
       974.0,    53.10;
       975.0,    54.60;
       976.0,    56.20;
       977.0,    57.70;
       978.0,    59.20;
       979.0,    60.80;
       980.0,    62.30;
       981.0,    63.80;
       982.0,    65.40;
       983.0,    66.90;
       984.0,    68.50;
       985.0,    70.00;
       986.0,    70.00;
       987.0,    70.00;
       988.0,    70.00;
       989.0,    70.00;
       990.0,    70.00;
       991.0,    70.00;
       992.0,    70.00;
       993.0,    70.00;
       994.0,    70.00;
       995.0,    70.00;
       996.0,    70.00;
       997.0,    70.00;
       998.0,    70.00;
       999.0,    70.00;
      1000.0,    70.00;
      1001.0,    70.00;
      1002.0,    70.00;
      1003.0,    70.00;
      1004.0,    70.00;
      1005.0,    70.00;
      1006.0,    70.00;
      1007.0,    70.00;
      1008.0,    70.00;
      1009.0,    70.00;
      1010.0,    70.00;
      1011.0,    70.00;
      1012.0,    70.00;
      1013.0,    70.00;
      1014.0,    70.00;
      1015.0,    70.00;
      1016.0,    70.00;
      1017.0,    70.00;
      1018.0,    70.00;
      1019.0,    70.00;
      1020.0,    70.00;
      1021.0,    70.00;
      1022.0,    70.00;
      1023.0,    70.00;
      1024.0,    70.00;
      1025.0,    70.00;
      1026.0,    70.00;
      1027.0,    70.00;
      1028.0,    70.00;
      1029.0,    70.00;
      1030.0,    70.00;
      1031.0,    70.00;
      1032.0,    70.00;
      1033.0,    70.00;
      1034.0,    70.00;
      1035.0,    70.00;
      1036.0,    70.90;
      1037.0,    71.70;
      1038.0,    72.60;
      1039.0,    73.40;
      1040.0,    74.30;
      1041.0,    75.10;
      1042.0,    76.00;
      1043.0,    76.90;
      1044.0,    77.70;
      1045.0,    78.60;
      1046.0,    79.40;
      1047.0,    80.30;
      1048.0,    81.10;
      1049.0,    82.00;
      1050.0,    82.90;
      1051.0,    83.70;
      1052.0,    84.60;
      1053.0,    85.40;
      1054.0,    86.30;
      1055.0,    87.10;
      1056.0,    88.00;
      1057.0,    88.90;
      1058.0,    89.70;
      1059.0,    90.60;
      1060.0,    91.40;
      1061.0,    92.30;
      1062.0,    93.10;
      1063.0,    94.00;
      1064.0,    94.90;
      1065.0,    95.70;
      1066.0,    96.60;
      1067.0,    97.40;
      1068.0,    98.30;
      1069.0,    99.10;
      1070.0,   100.00;
      1071.0,   100.00;
      1072.0,   100.00;
      1073.0,   100.00;
      1074.0,   100.00;
      1075.0,   100.00;
      1076.0,   100.00;
      1077.0,   100.00;
      1078.0,   100.00;
      1079.0,   100.00;
      1080.0,   100.00;
      1081.0,   100.00;
      1082.0,   100.00;
      1083.0,   100.00;
      1084.0,   100.00;
      1085.0,   100.00;
      1086.0,   100.00;
      1087.0,   100.00;
      1088.0,   100.00;
      1089.0,   100.00;
      1090.0,   100.00;
      1091.0,   100.00;
      1092.0,   100.00;
      1093.0,   100.00;
      1094.0,   100.00;
      1095.0,   100.00;
      1096.0,   100.00;
      1097.0,   100.00;
      1098.0,   100.00;
      1099.0,   100.00;
      1100.0,   100.00;
      1101.0,   101.00;
      1102.0,   102.00;
      1103.0,   103.00;
      1104.0,   104.00;
      1105.0,   105.00;
      1106.0,   106.00;
      1107.0,   107.00;
      1108.0,   108.00;
      1109.0,   109.00;
      1110.0,   110.00;
      1111.0,   111.00;
      1112.0,   112.00;
      1113.0,   113.00;
      1114.0,   114.00;
      1115.0,   115.00;
      1116.0,   116.00;
      1117.0,   117.00;
      1118.0,   118.00;
      1119.0,   119.00;
      1120.0,   120.00;
      1121.0,   120.00;
      1122.0,   120.00;
      1123.0,   120.00;
      1124.0,   120.00;
      1125.0,   120.00;
      1126.0,   120.00;
      1127.0,   120.00;
      1128.0,   120.00;
      1129.0,   120.00;
      1130.0,   120.00;
      1131.0,   117.50;
      1132.0,   115.00;
      1133.0,   112.50;
      1134.0,   110.00;
      1135.0,   107.50;
      1136.0,   105.00;
      1137.0,   102.50;
      1138.0,   100.00;
      1139.0,    97.50;
      1140.0,    95.00;
      1141.0,    92.50;
      1142.0,    90.00;
      1143.0,    87.50;
      1144.0,    85.00;
      1145.0,    82.50;
      1146.0,    80.00;
      1147.0,    76.30;
      1148.0,    72.50;
      1149.0,    68.80;
      1150.0,    65.00;
      1151.0,    61.30;
      1152.0,    57.50;
      1153.0,    53.80;
      1154.0,    50.00;
      1155.0,    45.00;
      1156.0,    40.00;
      1157.0,    35.00;
      1158.0,    30.00;
      1159.0,    25.00;
      1160.0,    20.00;
      1161.0,    15.00;
      1162.0,    10.00;
      1163.0,     5.00;
      1164.0,     0.00;
      1165.0,     0.00;
      1166.0,     0.00;
      1167.0,     0.00;
      1168.0,     0.00;
      1169.0,     0.00;
      1170.0,     0.00;
      1171.0,     0.00;
      1172.0,     0.00;
      1173.0,     0.00;
      1174.0,     0.00;
      1175.0,     0.00;
      1176.0,     0.00;
      1177.0,     0.00;
      1178.0,     0.00;
      1179.0,     0.00;
      1180.0,     0.00;
];
