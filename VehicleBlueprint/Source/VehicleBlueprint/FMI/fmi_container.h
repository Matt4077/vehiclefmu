#pragma once

#include <fmilib.h>
#include <vector>
#include <iostream>
#include <string>
#include "fmi_variable.h"
#include <fstream > 
#include <chrono>

#include <thread>


typedef std::chrono::high_resolution_clock::time_point Timer;
#define duration(a) std::chrono::duration_cast<std::chrono::nanoseconds>(a).count()
#define timeNow() std::chrono::high_resolution_clock::now()

class FmiContainer
{
public:
	FmiContainer(const char * fmuPath, const char * tmpPath, const char *fmuName, const char *instanceName, double step);
	~FmiContainer();
	static void logger(jm_callbacks* c, jm_string module, jm_log_level_enu_t log_level, jm_string message);

	bool load();
	bool init(bool visible = false, bool interactive = false, bool log_on_run = true);
	bool makeStep(bool log_all);
	//bool setInputs(int * varIndex, int size);
	double getCurrentTime();
	int getvariableVectorSize();
	void dumpVariableToFile();
	FMIvariable_b * getVariableByName(std::string name);
	FMIvariable_b * setControlVariable(std::string name);
	FMIvariable_b * setOutputVariable(std::string name);
	void start_simulation();
	void stop_simulation();
	void start_in_thread();

	// controll and output
	void setAccVariableName(std::string acc_name);
	void setBrakeVariableName(std::string brake_name);
	void setClutchVariableName(std::string clutch_name);
	void setGearVariableName(std::string gear_name);

	void setTorqueCommonVariableName(std::string torque_name);// one wheller
	void setResultingCommonWheelVelocityName(std::string result_wheel_velocity_name); // one wheller

	void setResultingWheelVelocityName(std::string result_velocity_name, int index);// multiple wheel model
	void setTorqueVariableName(std::string torque_name, int index); // multiple wheel model

	void setVehicleVelocityName(std::string vehicle_velocity_name); 
	void setBrakeOutName(std::string brake_out_name); 

	void setAcc(double val);
	void setBrake(double val);
	void setClutch(double val);
	void setGear(int val);

	void setResultingWheelVelocityCommon(double val);  // one wheller
	double getTorqueCommon(); // one wheller


	double getTorqueOnWheel(int wheelIndex); // multiple wheel model
	double getBrakeOut(); 
	void setResultingWheelVelocity(double val, int index); // multiple wheel model

	void setVehicleVelocity(double val); //velocity of the whole vehicle
	void copyFile(FString sourcePath, FString destinationPath);

private:
	bool running;
	FMIvariable<double> * acc;
	FMIvariable<double> * brake;
	FMIvariable<double> * clutch;
	FMIvariable<int> * gear;

	FMIvariable<double> * torque; //one wheeler model
	FMIvariable<double> * resultingWheelVelocity; //one wheeler model
	FMIvariable<double> * vehicleVelocity; //velocity of the whole vehicle
	FMIvariable<double> * brakeOut; //output brake to use Drive Cycle inside FMU
	std::vector<FMIvariable<double> *> resultingWheelVelocityVector; // multiple wheel model
	std::vector<FMIvariable<double> *> torqueOnWheels;// multiple wheel model


	std::thread the_thread;
	bool log_on_run;
	std::ofstream out;
	int varVectorSize;
	std::string fmuPath;
	std::string tmpPath;
	std::string fmuName;
	jm_callbacks callbacks;
	fmi_import_context_t* context;
	fmi1_import_t* fmu;
	fmi1_callback_functions_t callBackFunctions;
	fmi_version_enu_t version;
	fmi1_string_t fmuGUID;
	fmi1_real_t tstart;
	fmi1_real_t tcur;
	fmi1_real_t hstep;


	fmi1_string_t instanceName;
	fmi1_string_t fmuLocation;
	fmi1_string_t mimeType;

	std::vector<FMIvariable_b *> variables;
	std::vector<FMIvariable_b *> constants;
	// variable to set before each step.
	std::vector<FMIvariable_b *> control;
	// variable to get after each step.
	std::vector<FMIvariable_b *> outputs;

	bool fmuLoaded;
	bool fmuInitialised;
	void fileInit();

};

