#pragma once

#include <fmilib.h>
#include <string>
#include <vector>
#include <sstream>
#include <mutex>

class FMIvariable_b
{
public:
	static int size;
	FMIvariable_b(fmi1_import_variable_t *, std::string name, fmi1_base_type_enu_t type, std::string unit, unsigned int ind);
	~FMIvariable_b();
	std::string readFromFmu(fmi1_import_t* fmu, bool add_to_storage);
	std::string toString(int index);
	std::string getName();
	std::string getUnit();
	void clearVariableVector();
	double getLatest();
	bool writeToFmu(fmi1_import_t * fmu);
	void setAsControll();
	bool isControllVar();
protected:
	std::mutex variable_mtx;
	fmi1_import_variable_t * var;
	std::string name;
	std::string unit;
	fmi1_base_type_enu_t type;
	unsigned int ind;
	bool controlVal;
};

template <typename T>
class FMIvariable :public FMIvariable_b
{
private:
	std::vector<T>  vars;
	T latestVal;
	T setValue;

	std::mutex set_mtx;
public:
	FMIvariable<T>(fmi1_import_variable_t *var, std::string name, fmi1_base_type_enu_t type, std::string unit, unsigned int ind) : FMIvariable_b(var, name, type, unit, ind) { latestVal = 0; };
	~FMIvariable<T>() {};
	void add_data(T val, bool add_to_stoarage) {
		std::lock_guard<std::mutex> lock(variable_mtx);
		if (add_to_stoarage) vars.push_back(val);
		latestVal = val;
	};
	T get_data_at(int index) {
		std::lock_guard<std::mutex> lock(variable_mtx);
		return vars.at(index);
	};

	void clear() {
		std::lock_guard<std::mutex> lock(variable_mtx);
		this->vars.clear();
	};
	T getLatest() {
		std::lock_guard<std::mutex> lock(variable_mtx);
		//std::cout << this->name << " = " << this->latestVal << std::endl;
		return this->latestVal;
	}
	void setSetValue(T val) {
		std::lock_guard<std::mutex> lock(set_mtx);
		setValue = val;
	};
	T getSetValue() {
		std::lock_guard<std::mutex> lock(set_mtx);
		return setValue;
	}
};