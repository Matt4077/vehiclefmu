#pragma once

#include "CoreMinimal.h"

class FmiPowertrain
{
public:
	FmiPowertrain();
	void *initialize_powertrain(FString path, FString instanceName, double step_size);
	void deinitialize_powertrain(void * fmu);
	int start_powertrain(void* fmu, bool in_thred);
	int next_step(void* fmu);

	void setAcc(void* fmu, double val);
	void setBrake(void* fmu, double val);
	void setClutch(void* fmu, double val);
	void setResultingWheelVelocity(void* fmu, double val, int wheelIndex);
	void setVehicleVelocity(void* fmu_v, double val);

	void setGear(void* fmu, int val);
	double getTorque(void* fmu, int wheelIndex);
	double getBrakeOut(void* fmu);

	double getCurrentTime(void* fmu);

	FString getWorkingDirectory();

private:

};