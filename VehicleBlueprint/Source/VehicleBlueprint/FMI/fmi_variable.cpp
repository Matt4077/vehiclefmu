#include "fmi_variable.h"
#include <iostream>

int FMIvariable_b::size = 40;

FMIvariable_b::FMIvariable_b(fmi1_import_variable_t *, std::string name, fmi1_base_type_enu_t type, std::string unit, unsigned int ind)
{
	this->var = var;
	this->name = name;
	this->type = type;
	this->ind = ind;
	this->unit = unit;
}

FMIvariable_b::~FMIvariable_b()
{
}

//read outputs from the fmu
std::string FMIvariable_b::readFromFmu(fmi1_import_t * fmu, bool add_to_storage)
{
	std::stringstream ss;
	switch (this->type) {
	case fmi1_base_type_real:
	{
		fmi1_real_t rvalue;
		fmi1_status_t fmistatus = fmi1_import_get_real(fmu, &this->ind, 1, &rvalue);
		((FMIvariable<double> *)this)->add_data(rvalue, add_to_storage);
		ss << rvalue;
	}
	break;

	case fmi1_base_type_int:
	case fmi1_base_type_enum:
	{
		fmi1_integer_t ivalue;
		fmi1_status_t fmistatus = fmi1_import_get_integer(fmu, &this->ind, 1, &ivalue);
		((FMIvariable<int> *)this)->add_data(ivalue, add_to_storage);
		ss << ivalue;
	}
	break;

	case fmi1_base_type_bool:
	{
		fmi1_boolean_t bvalue;
		fmi1_status_t fmistatus = fmi1_import_get_boolean(fmu, &this->ind, 1, &bvalue);
		((FMIvariable<bool> *)this)->add_data(bvalue == fmi1_true, add_to_storage);
		ss << (bvalue == '1');
	}
	break;

	case fmi1_base_type_str:
		// Not supported yet
	default:
		break;


	}
	return ss.str();
}

std::string FMIvariable_b::toString(int index)
{
	std::stringstream ss;

	switch (this->type) {
	case fmi1_base_type_real:
	{
		ss << ((FMIvariable<double> *)this)->get_data_at(index);
		return ss.str();
	}
	break;

	case fmi1_base_type_int:
	{
		ss << ((FMIvariable<int> *)this)->get_data_at(index);
		return ss.str();
	}
	break;

	case fmi1_base_type_bool:
	{
		ss << ((FMIvariable<bool> *)this)->get_data_at(index);
		return ss.str();
	}
	break;



	case fmi1_base_type_enum:
		ss << ((FMIvariable<int> *)this)->get_data_at(index);
		return ss.str();
		break;
	case fmi1_base_type_str:
		// Not supported yet
	default:
		return "";
		break;
	}
}

std::string FMIvariable_b::getName()
{
	return this->name;
}

std::string FMIvariable_b::getUnit()
{
	return this->unit;
}

//clear the storing variables
void FMIvariable_b::clearVariableVector()
{
	switch (this->type) {
	case fmi1_base_type_real:
		((FMIvariable<double> *)this)->clear();
		break;
	case fmi1_base_type_int:
		((FMIvariable<int> *)this)->clear();
		break;
	case fmi1_base_type_bool:
		((FMIvariable<bool> *)this)->clear();
		break;
	case fmi1_base_type_enum:
		((FMIvariable<int> *)this)->clear();
		break;
	case fmi1_base_type_str:
		// NOT SUPPORTED YET
	default:
		break;
	}
}

//get the value stored in the Latest
double FMIvariable_b::getLatest()
{
	switch (this->type) {
	case fmi1_base_type_real:
		return ((FMIvariable<double> *)this)->getLatest();
	case fmi1_base_type_int:
		return (double)((FMIvariable<int> *)this)->getLatest();
	case fmi1_base_type_bool:
		return (double)((FMIvariable<bool> *)this)->getLatest();
	case fmi1_base_type_enum:
		return (double)((FMIvariable<int> *)this)->getLatest();
	case fmi1_base_type_str:
		// NOT SUPPORTED YET
	default:
		return 0.0;
	}
}

//write inputs to the fmu
bool FMIvariable_b::writeToFmu(fmi1_import_t * fmu)
{
	/***
	Function to be used only in FmiContainer.... if variable was added to the controll
	***/

	fmi1_status_t status;
	switch (this->type) {
	case fmi1_base_type_real:
	{
		const double  val = ((FMIvariable<double> *)this)->getSetValue();
		status = fmi1_import_set_real(fmu, &this->ind, 1, &val);
		//std::cout << this->name << " -> " << ((FMIvariable<double> *)this)->getSetValue() << std::endl;
		break;
	}
	case fmi1_base_type_enum:
	case fmi1_base_type_int:
	{
		const int  val = ((FMIvariable<int> *)this)->getSetValue();
		status = fmi1_import_set_integer(fmu, &this->ind, 1, &val);
		//std::cout << this->name << " -> " << ((FMIvariable<int> *)this)->getSetValue() << std::endl;

		break;
	}
	case fmi1_base_type_bool:
	{
		fmi1_boolean_t  val = (((FMIvariable<bool> *)this)->getSetValue()) ? fmi1_true : fmi1_false;
		status = fmi1_import_set_boolean(fmu, &this->ind, 1, &val);
		//std::cout << this->name << " -> " << ((FMIvariable<bool> *)this)->getSetValue() << std::endl;
		break;
	}
	case fmi1_base_type_str:
	default:
		status = fmi1_status_error;
		break;
	}
	return status == fmi1_status_ok;
}

void FMIvariable_b::setAsControll()
{
	this->controlVal = true;
}

bool FMIvariable_b::isControllVar()
{
	return this->controlVal;
}
