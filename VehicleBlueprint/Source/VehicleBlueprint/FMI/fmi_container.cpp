#include "fmi_container.h"
#include <algorithm>
#include "EngineGlobals.h"
#include "Paths.h"
#include "PlatformFilemanager.h"
#include "PlatformFile.h"
#include "FileManager.h"
#include "XmlParser/Public/FastXml.h"
#include "XmlParser/Public/XmlParser.h"
#include "XmlParser/Public/XmlFile.h"

FmiContainer::FmiContainer(const char * fmuPath, const char * tmpPath, const char * fmuName,const char * instanceName, double step)
{
	resultingWheelVelocityVector.assign(4, nullptr);
	torqueOnWheels.assign(4, nullptr);

	this->fmuPath = fmuPath;
	this->tmpPath = tmpPath;
	this->fmuName = fmuName;
	this->instanceName = instanceName;
	this->tstart = 0.0;
	this->tcur = 0.0;
	this->hstep = step;

	this->fmuLocation = "";
	this->mimeType = "";

	

	fmuLoaded = false;
	fmuInitialised = false;
	
	callbacks.malloc = malloc;
	callbacks.calloc = calloc;
	callbacks.realloc = realloc;
	callbacks.free = free;
	callbacks.logger = FmiContainer::logger;
	callbacks.log_level = jm_log_level_debug;
	callbacks.context = 0;

	callBackFunctions.logger = fmi1_log_forwarding;
	callBackFunctions.allocateMemory = calloc;
	callBackFunctions.freeMemory = free;
	this->varVectorSize = 0;
}

FmiContainer::~FmiContainer()
{
	fmi1_import_destroy_dllfmu(fmu);
	fmi1_import_free(fmu);
	fmi_import_free_context(context);
	if (!log_on_run) {
		out.close();
	}
}

void FmiContainer::logger(jm_callbacks * c, jm_string module, jm_log_level_enu_t log_level, jm_string message)
{
	std::string mod = module;
	FString modul = mod.c_str();
	std::string mess = message;
	FString messag = mess.c_str();

	UE_LOG(LogTemp, Warning, TEXT("module = %s, log level = %s: %s\n"), *modul, jm_log_level_to_string(log_level), *messag);
}

class XMLParser : public IFastXmlCallback
{
public:
	//IFastXmlCallback
	bool ProcessXmlDeclaration(const TCHAR* ElementData, int32 XmlFileLineNumber);
	bool ProcessElement(const TCHAR* ElementName, const TCHAR* ElementData, int32 XmlFileLineNumber);
	bool ProcessAttribute(const TCHAR* AttributeName, const TCHAR* AttributeValue);
	bool ProcessClose(const TCHAR* Element);
	bool ProcessComment(const TCHAR* Comment);
	bool checker = false;
	int counter = 0;
};

FString auxFileName;

bool FmiContainer::load()
{
	context = fmi_import_allocate_context(&callbacks);
	version = fmi_import_get_fmi_version(context, fmuPath.c_str(), tmpPath.c_str());
	//std::string dllName = (fmuPath.substr(0, fmuPath.size() - 4)).erase;
	//FString dllName = FString(fmuName.c_str()) + ".dll";
	//UE_LOG(LogTemp, Warning, TEXT("dllName is: %s"), *dllName);
	
	FString ProjectPath = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*FPaths::ProjectDir());
	FString AbsoluteSourcePath1 = ProjectPath + "auxiliary/tmp/binaries/win64/istandard_c.dll";
	FString AbsoluteSourcePath2 = ProjectPath + "auxiliary/tmp/binaries/win64/ipowertrain_c.dll";
	//FString AbsoluteSourcePath3 = ProjectPath + "auxiliary/tmp/binaries/win64/" + dllName;
	FString AbsoluteSourcePath4 = ProjectPath + "auxiliary/tmp/resources";
	FString AbsoluteSourcePath5 = ProjectPath + "Binaries/Win64/fmilib_shared.dll";

	FString EnginePath = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*FPaths::EngineDir());
	FString AbsoluteDestinationPath1 = EnginePath + "Binaries/Win64/istandard_c.dll";
	FString AbsoluteDestinationPath2 = EnginePath + "Binaries/Win64/ipowertrain_c.dll";
	//FString AbsoluteDestinationPath3 = EnginePath + "Binaries/Win64/" + dllName;
	FString AbsoluteDestinationPath5 = EnginePath + "Binaries/Win64/fmilib_shared.dll";

	copyFile(AbsoluteSourcePath1, AbsoluteDestinationPath1);
	copyFile(AbsoluteSourcePath2, AbsoluteDestinationPath2);
	copyFile(AbsoluteSourcePath5, AbsoluteDestinationPath5);
	
	FString XML = ProjectPath + "auxiliary/tmp/modelDescription.xml";
	FText outErrorMess;
	int32 outErrorLine;
	IFastXmlCallback* callback = new XMLParser();
	UE_LOG(LogTemp, Warning, TEXT("TEST before XML Parser\n"));
	bool success = FFastXml::ParseXmlFile(callback, *XML, TEXT(""), nullptr, false, false, outErrorMess, outErrorLine);
	if(!success)
	UE_LOG(LogTemp, Warning, TEXT("XML Parser error line %d: %s\n"), outErrorLine, *outErrorMess.ToString());

	UE_LOG(LogTemp, Warning, TEXT("Last auxiliary file name is: %s\n"), *auxFileName);

	fmu = fmi1_import_parse_xml(context, tmpPath.c_str());

	if (fmu) {
		fmuLoaded = true;
	}
	else {
		UE_LOG(LogTemp, Fatal, TEXT("Error parsing XML, exiting\n"));
	}

	if (fmi1_import_create_dllfmu(fmu, callBackFunctions, 0) == jm_status_error) {
		std::string err = fmi1_import_get_last_error(fmu);
		FString error = err.c_str();

		UE_LOG(LogTemp, Fatal, TEXT("Could not create the DLL loading mechanism(C-API) (error: %s).\n"), *error);
		fmuLoaded = false;
	}

	return fmuLoaded;
}



bool FmiContainer::init(bool visible, bool interactive, bool log_on_run)
{
	this->log_on_run = log_on_run;
	fmi1_fmu_kind_enu_t fmu_type = fmi1_import_get_fmu_kind(fmu);
	std::string plaform = fmi1_import_get_types_platform(fmu);

	if (fmi1_import_instantiate_slave(fmu, instanceName, fmuLocation, mimeType, this->hstep, visible, interactive) == jm_status_error) {
		UE_LOG(LogTemp, Warning, TEXT("fmi1_import_instantiate_model failed\n"));
		fmuInitialised = false;
		return false;
	}
	if (fmi1_import_initialize_slave(fmu, tstart, false, 18000) != fmi1_status_ok) {
		UE_LOG(LogTemp, Warning, TEXT("fmi1_import_initialize_slave failed\n"));
		fmuInitialised = false;
		return false;
	}
	fmuInitialised = true;
	fmi1_import_variable_list_t * vars = fmi1_import_get_variable_list(fmu);
	int size = fmi1_import_get_variable_list_size(vars);
	int aditionalmodels = fmi1_import_get_number_of_additional_models(fmu);
	for (int i = 0; i < size; i++) {
		fmi1_import_variable_t * var = fmi1_import_get_variable(vars, i);
		std::string name = fmi1_import_get_variable_name(var);
		if (name.find("_") == 0) continue;
		std::string unit = "";
		fmi1_base_type_enu_t type = fmi1_import_get_variable_base_type(var);
		unsigned int ref = fmi1_import_get_variable_vr(var);
		FMIvariable_b  * var_cont;
		int variability = fmi1_import_get_variability(var);
		int casaulty = fmi1_import_get_causality(var);
		const char * description = fmi1_import_get_variable_description(var);
		bool add = false;
		switch (type) {
		case fmi1_base_type_real:
		{
			fmi1_import_unit_t * unit_t = fmi1_import_get_real_variable_unit((fmi1_import_real_variable_t *)var);
			std::string unit = "";
			if (unit_t) {
				unit = fmi1_import_get_unit_name(unit_t);
			}
			var_cont = new FMIvariable<double>(var, name, type, unit, ref);


		}
		break;
		case fmi1_base_type_enum:
		case fmi1_base_type_int:
		{
			var_cont = new FMIvariable<int>(var, name, type, unit, ref);
		}
		break;

		case fmi1_base_type_bool:
		{
			var_cont = new FMIvariable<bool>(var, name, type, unit, ref);
		}
		break;
		break;
		case fmi1_base_type_str:
		default:
			var_cont = NULL;
			break;


		}
		if (var_cont) {
			if (variability < 2) {
				this->constants.push_back(var_cont);
			}
			else {
				var_cont->readFromFmu(fmu, false);
				this->variables.push_back(var_cont);
			}
		}
	}
	this->fileInit();
	return true;
}

bool FmiContainer::makeStep(bool log_all)
{
	if (this->fmuInitialised && this->fmuLoaded) {
		Timer t1 = timeNow();
		//std::cout << "Step (" << tcur << " , " << hstep << ")";
		fmi1_status_t fmistatus;
		fmi1_boolean_t newStep = fmi1_true;
		// Setting new values if any
		for (int i = 0; i < control.size(); i++) {
			control.at(i)->writeToFmu(fmu);
		}
		fmistatus = fmi1_import_do_step(fmu, tcur, hstep, newStep);
		if (fmistatus == fmi1_status_ok) {
			if (log_all) {
				std::string var_str = "";
				for (int i = 0; i < variables.size(); i++) {
					FMIvariable_b * var = variables.at(i);
					var_str = var->readFromFmu(fmu, !log_on_run);
					if (log_on_run) {
						const char * del = (i < variables.size() - 1) ? "," : "\n";
						out << var_str << del;
					}
				}
				varVectorSize++;
			}

			double time_ms = duration(timeNow() - t1)*1e-9;
			if (time_ms > hstep) {
				//Adjusting timestep 
				//hstep += (time_ms - hstep)*0.1;
				std::cout << "Took to long :" << time_ms << " [s] instead of " << hstep << std::endl;
				tcur += time_ms;
			}
			else {
				tcur += hstep;
			}
			for (int i = 0; i < outputs.size(); i++) {
				outputs.at(i)->readFromFmu(fmu, false);
				std::cout << outputs.at(i)->getName() << "=" << outputs.at(i)->getLatest() << std::endl;
			}

			std::cout << " SUCCESS in :" << duration(timeNow() - t1)*1e-6 << " [ms] " << std::endl;
			return true;
		}
		else {
			std::cout << " Failed" << std::endl;
		}
	}
	return false;
}

double FmiContainer::getCurrentTime()
{
	return tcur;
}

int FmiContainer::getvariableVectorSize()
{
	return varVectorSize;
}

void FmiContainer::dumpVariableToFile()
{
	if (!log_on_run) {
		std::string filename = this->tmpPath + "/results.csv";
		out.open(filename, std::ofstream::out | std::ofstream::app);
		for (int val_index = 0; val_index < varVectorSize; val_index++) {
			for (int i = 0; i < variables.size(); i++) {
				const char * del = (i < variables.size() - 1) ? "," : "\n";
				out << variables.at(i)->toString(val_index) << del;
				if (val_index == varVectorSize - 1) {
					variables.at(i)->clearVariableVector();
				}
			}
		}
		varVectorSize = 0;
		out.close();
		UE_LOG(LogTemp, Warning, TEXT("Variables were added to the results.csv"));
	}
}

FMIvariable_b * FmiContainer::getVariableByName(std::string name)
{
	for (int i = 0; i < variables.size(); i++) {
		FMIvariable_b *  var = variables.at(i);
		if (var->getName().compare(name) == 0) {
			return var;
		}
	}
	return NULL;
}

FMIvariable_b * FmiContainer::setControlVariable(std::string name)
{
	FMIvariable_b * var = getVariableByName(name);
	if (var) {
		var->setAsControll();
		control.push_back(var);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT(" can not find"));// %s"), name);
		return nullptr;
	}
	return var;
}

FMIvariable_b * FmiContainer::setOutputVariable(std::string name)
{
	FMIvariable_b * var = getVariableByName(name);
	if (var) {
		outputs.push_back(var);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT(" can not find"))// %s"), name);
		return nullptr;

	}
	return var;
}

void FmiContainer::start_simulation()
{
	running = true;
	Timer t1 = timeNow();
	while (running) {
		makeStep(true);
		auto dif = (tcur * 1e9) - duration(timeNow() - t1);
		if (dif > 0) {
			//std::cout << "Sleeping for : " << dif << std::endl;
			std::this_thread::sleep_for(std::chrono::nanoseconds((int)dif));
		}
	}
}

void FmiContainer::stop_simulation()
{
	running = false;
	dumpVariableToFile();
}

void FmiContainer::start_in_thread()
{
	the_thread = std::thread(&FmiContainer::start_simulation, this);
}

void FmiContainer::setAccVariableName(std::string acc_name)
{
	FMIvariable<double> * v = (FMIvariable<double> *) setControlVariable(acc_name);
	if (v) {
		acc = v;
		acc->setSetValue(0.0);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("-------Can not set acc"));
}

void FmiContainer::setBrakeVariableName(std::string brake_name)
{
	FMIvariable<double> * v = (FMIvariable<double> *) setControlVariable(brake_name);
	if (v) {
		brake = v;
		brake->setSetValue(0.0);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("-------Can not set brake"));
}

void FmiContainer::setClutchVariableName(std::string clutch_name)
{
	FMIvariable<double> * v = (FMIvariable<double> *) setControlVariable(clutch_name);
	if (v) {
		clutch = v;
		clutch->setSetValue(0.0);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("-------Can not set clutch"));
}

void FmiContainer::setGearVariableName(std::string gear_name)
{
	FMIvariable<int> * v = (FMIvariable<int> *) setControlVariable(gear_name);
	if (v) {
		gear = v;
		gear->setSetValue(0.0);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("-------Can not set gear"));
}

void FmiContainer::setTorqueCommonVariableName(std::string torque_name)
{
	FMIvariable<double> * v = (FMIvariable<double> *) setOutputVariable(torque_name);
	if (v) {
		torque = v;
		torque->add_data(0.0, false);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("-------Can not set torque"));
	}
}

void FmiContainer::setResultingCommonWheelVelocityName(std::string result_wheel_velocity_name)
{
	FMIvariable<double> * v = (FMIvariable<double> *) setControlVariable(result_wheel_velocity_name);
	if (v) {
		resultingWheelVelocity = v;
		resultingWheelVelocity->setSetValue(0.0);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("-------Can not set wheel velocity"));
		
}

void FmiContainer::setResultingWheelVelocityName(std::string result_velocity_name, int index)
{
	FMIvariable<double> * v = (FMIvariable<double> *) setControlVariable(result_velocity_name);
	if (v) {
		resultingWheelVelocityVector.at(index) = v;
		resultingWheelVelocityVector.at(index)->setSetValue(0.0);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("-------Can not set wheel velocity_name")); //%d"), index);
		
}

void FmiContainer::setTorqueVariableName(std::string torque_name, int index)
{
	FMIvariable<double> * v = (FMIvariable<double> *) setOutputVariable(torque_name);
	if (v) {
		torqueOnWheels.at(index) = v;
		torqueOnWheels.at(index)->add_data(0.0, false);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("-------Can not set wheel_torque_name")); //%d"), index);
}

void FmiContainer::setVehicleVelocityName(std::string vehicle_velocity_name)
{
	FMIvariable<double> * v = (FMIvariable<double> *) setControlVariable(vehicle_velocity_name);
	if (v) {
		vehicleVelocity = v;
		vehicleVelocity->setSetValue(0.0);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("-------Can not set wheel velocity"));
}

void FmiContainer::setBrakeOutName(std::string brake_out_name)
{
	FMIvariable<double> * v = (FMIvariable<double> *) setControlVariable(brake_out_name);
	if (v) {
		brakeOut = v;
		brakeOut->setSetValue(0.0);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("-------Can not set brake output name"));
}

void FmiContainer::setAcc(double val)
{
	acc->setSetValue(val);
}

void FmiContainer::setBrake(double val)
{
	brake->setSetValue(val);
}

void FmiContainer::setClutch(double val)
{
	if (clutch) {
		clutch->setSetValue(val);
	}
}

void FmiContainer::setGear(int val)
{
	if (gear) {
		gear->setSetValue(val);
	}
}

void FmiContainer::setResultingWheelVelocityCommon(double val)
{
	if (resultingWheelVelocity) {
		resultingWheelVelocity->setSetValue(val);
	}
}

double FmiContainer::getTorqueCommon()
{
	if (torque) {
		return torque->getLatest();
	}
	return 0.0;
}

double FmiContainer::getTorqueOnWheel(int wheelIndex)
{
	if (torqueOnWheels.at(wheelIndex)) {
		return torqueOnWheels.at(wheelIndex)->getLatest();
	}
	else {
		return 0.0;
	}
}

double FmiContainer::getBrakeOut()
{
	if (brakeOut) {
		return brakeOut->getLatest();
	}
	return 0.0;
}

void FmiContainer::setResultingWheelVelocity(double val, int index)
{
	if (resultingWheelVelocityVector.at(index))
		resultingWheelVelocityVector.at(index)->setSetValue(val);
}

void FmiContainer::setVehicleVelocity(double val)
{
	if (vehicleVelocity) {
		vehicleVelocity->setSetValue(val);
	}
}

void FmiContainer::fileInit()
{
	std::string filename = this->tmpPath + "/results.csv";
	out.open(filename, std::ofstream::out);
	for (int i = 0; i < variables.size(); i++) {
		std::string name = variables.at(i)->getName();
		std::string unit = variables.at(i)->getUnit();
		std::replace(name.begin(), name.end(), ',', '.'); // replace all ',' to '-'
		std::replace(unit.begin(), unit.end(), ',', '.'); // replace all ',' to '-'
		const char * del = (i < variables.size() - 1) ? "," : "\n";
		out << name << "[" << unit << "]" << del;
	}

	if (!log_on_run) {
		out.close();
	}
}

void FmiContainer::copyFile(FString sourcePath, FString destinationPath)
{
	if (!FPlatformFileManager::Get().GetPlatformFile().FileExists(*sourcePath))
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not find file"));
	}
	if (!FPlatformFileManager::Get().GetPlatformFile().CopyFile(*destinationPath, *sourcePath))
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not copy file"));
	}
}

bool XMLParser::ProcessXmlDeclaration(const TCHAR * ElementData, int32 XmlFileLineNumber)
{
	UE_LOG(LogTemp, Warning, TEXT("Parser declaration. Data: %s, Line: %d"), ElementData, XmlFileLineNumber);
	return true;
}

bool XMLParser::ProcessElement(const TCHAR * ElementName, const TCHAR * ElementData, int32 XmlFileLineNumber)
{
	UE_LOG(LogTemp, Warning, TEXT("Parser element. Name: %s, Data: %s, Line: %d"), ElementName, ElementData, XmlFileLineNumber);
	FString nameString = "String";
	const TCHAR* nameStringChar = *nameString;
	return true;
}

bool XMLParser::ProcessAttribute(const TCHAR * AttributeName, const TCHAR * AttributeValue)
{
	//UE_LOG(LogTemp, Warning, TEXT("Parser attribute. Name: %s, Value: %s"), AttributeName, AttributeValue);
	FString name = "description";
	const TCHAR* nameChar = *name;

	FString value = "Data file name";
	const TCHAR* valueChar = *value;

	FString nameStart = "start";
	const TCHAR* nameStartChar = *nameStart; 
	//UE_LOG(LogTemp, Warning, TEXT("CHAR Values are: %s, %s, %s"), nameChar, valueChar, nameStartChar);
	if (_tcscmp(AttributeName,nameChar) == 0 && _tcscmp(AttributeValue, valueChar) == 0)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Hello from inside of IF"));
		checker = true;
		counter = 0;
	}
	if (counter < 6 && checker && _tcscmp(AttributeName, nameStartChar) == 0)
	{
		auxFileName = AttributeValue;
		checker = false;
		//UE_LOG(LogTemp, Warning, TEXT("Hello from inside of second IF"));
	}
	counter++;
	return true;
}

bool XMLParser::ProcessClose(const TCHAR * Element)
{
	//UE_LOG(LogTemp, Warning, TEXT("Parser close. Element: %s"), Element);
	return true;
}

bool XMLParser::ProcessComment(const TCHAR * Comment)
{
	//UE_LOG(LogTemp, Warning, TEXT("Parser comment: %s"), Comment);
	return true;
}
