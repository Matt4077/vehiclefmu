#include "fmi_powertrain.h"
#include "fmi_container.h"
#include "fmi_variable.h"
#define WIN32_LEAN_AND_MEAN 
#include <Windows.h>
#include "Paths.h"
#include "FileManager.h"

FmiPowertrain::FmiPowertrain()
{
}

void *FmiPowertrain::initialize_powertrain(FString pathToSettings, FString instance_name, double step_size) {
	FString w_dir_rel = getWorkingDirectory();

	FString PathTest1 = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*FPaths::EngineDir());
	UE_LOG(LogTemp, Warning, TEXT("the path engineDir is: %s"), *PathTest1);
	
	FString binarDir = PathTest1 + "Binaries/Win64";

	FString w_dir_full = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*w_dir_rel);
	UE_LOG(LogTemp, Warning, TEXT("the path to the directory is: %s"), *w_dir_full);
	//FString w_dirTest = "C:/Reps/Vehicle_fmu/VehicleBlueprint/"; //change after pull from source control

	FString fmuPath = w_dir_full + "auxiliary/" + pathToSettings + ".fmu";
	std::string ansi_fmuPath(TCHAR_TO_UTF8(*fmuPath));
	UE_LOG(LogTemp, Warning, TEXT("the path to fmu is: %s"), *fmuPath);

	FString tmpPath = w_dir_full + "auxiliary/tmp";
	std::string ansi_tmpPath(TCHAR_TO_UTF8(*tmpPath));
	UE_LOG(LogTemp, Warning, TEXT("the path to tmp is: %s"), *tmpPath);

	std::string ansi_instanceName(TCHAR_TO_UTF8(*instance_name));
	std::string ansi_fmuName(TCHAR_TO_UTF8(*pathToSettings));

	bool multipleWheels = true;
	bool clutch = false;
	bool gear = false;

	std::string clutchName = " ";
	std::string gearName = " ";

	std::string accName = "Accn_in";
	std::string brakeName = "Brake_in";
	std::string velocityName = "Vel_in";
	std::string brakeOutName = "Brake_out";
	std::string wheelVelocityName1 = "FRSpeed";
	std::string wheelVelocityName2 = "FLSpeed";
	std::string wheelVelocityName3 = "RRSpeed";
	std::string wheelVelocityName4 = "RLSpeed";
	std::string torqueName1 = "Trq_FR";
	std::string torqueName2 = "Trq_FL";
	std::string torqueName3 = "Trq_RR";
	std::string torqueName4 = "Trq_RL";
	
	
	FmiContainer * fmu = new FmiContainer(ansi_fmuPath.c_str(), ansi_tmpPath.c_str(), ansi_fmuName.c_str(), ansi_instanceName.c_str(), step_size);
	
	fmu->load();
	
	if (!fmu->init(false, true, false)) {
		UE_LOG(LogTemp,Warning, TEXT("Can not load fmu"))
	}
	
	fmu->setAccVariableName(accName);
	fmu->setBrakeVariableName(brakeName);
	fmu->setVehicleVelocityName(velocityName);
	//fmu->setBrakeOutName(brakeOutName); //enable when using PowertrainNEDC.fmu

	if (multipleWheels) {
		for (int i = 0; i < 2; i++) {
			if (i == 0) {
				fmu->setTorqueVariableName(torqueName1, i);
				fmu->setResultingWheelVelocityName(wheelVelocityName1, i);
			}
			else if (i == 1) {
				fmu->setTorqueVariableName(torqueName2, i);
				fmu->setResultingWheelVelocityName(wheelVelocityName2, i);
			}
			else if (i == 2) {
				fmu->setTorqueVariableName(torqueName3, i);
				fmu->setResultingWheelVelocityName(wheelVelocityName3, i);
			}
			else {
				fmu->setTorqueVariableName(torqueName4, i);
				fmu->setResultingWheelVelocityName(wheelVelocityName4, i);
			}
		}
	}
	else {
		fmu->setTorqueCommonVariableName(torqueName1);  //TODO probably better to make separate parameter for 1 wheel drivetrain
		fmu->setResultingCommonWheelVelocityName(wheelVelocityName1); //TODO probably better to make separate parameter for 1 wheel drivetrain
	}

	if (clutch) {
		fmu->setClutchVariableName(clutchName);
	}

	if (gear) {
		fmu->setGearVariableName(gearName);
	}
	return fmu;
}

void FmiPowertrain::deinitialize_powertrain(void * val)
{
	if (FmiContainer * fmu = (FmiContainer *)val) {
		fmu->stop_simulation();
		delete fmu;
	}
}

int FmiPowertrain::start_powertrain(void * fmu_v, bool in_thread)
{
	if (FmiContainer * fmu = (FmiContainer *)fmu_v) {
		if (in_thread) {
			fmu->start_in_thread();
			return 1;
		}
		return 1;

	}
	return 0;
}

int FmiPowertrain::next_step(void * fmu_v)
{
	if (FmiContainer * fmu = (FmiContainer *)fmu_v) {
		return fmu->makeStep(true); //true to dump variables to *.csv
	}
	return -1;
}

void FmiPowertrain::setAcc(void * fmu_v, double val)
{
	if (FmiContainer * fmu = (FmiContainer *)fmu_v) {
		fmu->setAcc(val);
	}
}

void FmiPowertrain::setBrake(void * fmu_v, double val)
{
	if (FmiContainer * fmu = (FmiContainer *)fmu_v) {
		fmu->setBrake(val);
	}
}

void FmiPowertrain::setClutch(void * fmu_v, double val)
{
	if (FmiContainer * fmu = (FmiContainer *)fmu_v) {
		fmu->setClutch(val);
	}
}

void FmiPowertrain::setResultingWheelVelocity(void * fmu_v, double val, int wheelIndex)
{
	if (FmiContainer * fmu = (FmiContainer *)fmu_v) {
		return fmu->setResultingWheelVelocity(val, wheelIndex);
	}
}

void FmiPowertrain::setVehicleVelocity(void * fmu_v, double val)
{
	if (FmiContainer * fmu = (FmiContainer *)fmu_v) {
		return fmu->setVehicleVelocity(val);
	}
}

void FmiPowertrain::setGear(void * fmu_v, int val)
{
	if (FmiContainer * fmu = (FmiContainer *)fmu_v) {
		fmu->setGear(val);
	}
}

double FmiPowertrain::getTorque(void * fmu_v, int wheelIndex)
{
	if (FmiContainer * fmu = (FmiContainer *)fmu_v) {
		return fmu->getTorqueOnWheel(wheelIndex);
	}
	else {
		return 0.0;
	}
}

double FmiPowertrain::getBrakeOut(void * fmu_v)
{
	if (FmiContainer * fmu = (FmiContainer *)fmu_v) {
		return fmu->getBrakeOut();
	}
	else {
		return 0.0;
	}
}

double FmiPowertrain::getCurrentTime(void * fmu_v)
{
	if (FmiContainer * fmu = (FmiContainer *)fmu_v) {
		return fmu->getCurrentTime();
	}
	return -1;
}


FString FmiPowertrain::getWorkingDirectory()
{
	FString Path = FPaths::GameDir();
	return Path;
}



