// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FmuFunction.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VEHICLEBLUEPRINT_API UFmuFunction : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFmuFunction();

	//similar to the usage in SimpleWheeledVehicleComponent.h, probably for function to be able to be shown in the event graph
	/*Load FMU with its inputs and outputs*/
	UFUNCTION(BlueprintCallable, Category = Vehicle)
	void SimulateFmu(float tyreRadius, float BrakingForce, float Velocity, float AccIn, float BrakeIn, float AngV_FL, float AngV_FR, float &TrqFL_out, float &TrqFR_out, float &fmuTime, float &brakeOut);
	UFUNCTION(BlueprintCallable, Category = Vehicle)
	void DestroyFMU();
	UFUNCTION(BlueprintCallable, Category = Vehicle)
	void SetFMUBP(FString fmuName, FString instanceName, float step_size);
	UFUNCTION(BlueprintCallable, Category = Vehicle)
	void StartInThread();
	//
	const static bool firstFrame = true;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
