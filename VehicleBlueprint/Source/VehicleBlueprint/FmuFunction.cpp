// Fill out your copyright notice in the Description page of Project Settings.

#include "FmuFunction.h"
#include "FmuImport.h"
#include <fmilib.h>
#include <FMI/fmi_powertrain.h>

/*
To distribute this game: 
1) build the game with proper fmu, click play the game in editor, the game will crash but necessary dlls and potential *.m files will be generated
2) package the game using file->package->Win64
3) go to '"yourProject"/Binaries/Win64' and copy "fmilib_shared.dll" to generated package 'WindowsNoEditor/"yourProject"/Binaries/Win64'
4) in the generated package go to 'WindowsNoEditor/"yourProject"/Binaries/Win64' and put there *.m files from your 'auxiliary/tmp/resources' and dlls from 'auxiliary/tmp/binaries/win64'
5) then in the package go to 'WindowsNoEditor/"yourProject"' and create here folder named "auxiliary", put there your fmu and associated files
6) create folder named "tmp" in 'WindowsNoEditor/"yourProject"/auxiliary'
7) ready to run!
*/

// Sets default values for this component's properties
UFmuFunction::UFmuFunction()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
	
	// ...
}


// Called when the game starts
void UFmuFunction::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UFmuFunction::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

//needs to be global because it has initialization function that needs to be called only once
FmiPowertrain fmiPowertrain;
static void *fmu;


// Function representing the FMU block with its inputs and outputs, implements the FMU import and simulation (& represents output for the EventGraph)
//TODO make parameters for this block editable in the event graph similar to what is in now in *.ini file
void UFmuFunction::SimulateFmu(float tyreRadius, float BrakingForce,float Velocity, float AccIn, float BrakeIn, float AngV_FL, float AngV_FR, float &TrqFL_out, float &TrqFR_out, float &fmuTime, float &brakeOut_out)
{
	//definition of variables used to transfer torque to the front wheels
	float TrqFL;
	float TrqFR;
	float BrakeOut = 0;

	fmiPowertrain.setAcc(fmu, AccIn);
	fmiPowertrain.setBrake(fmu, BrakeIn);
	
	TrqFR = fmiPowertrain.getTorque(fmu, 0);
	TrqFL = fmiPowertrain.getTorque(fmu, 1);
	//BrakeOut = fmiPowertrain.getBrakeOut(fmu); //enable when using PowertrainNEDC.fmu
	
	fmiPowertrain.setVehicleVelocity(fmu, Velocity/100);
	fmiPowertrain.setResultingWheelVelocity(fmu, AngV_FR, 0);
	fmiPowertrain.setResultingWheelVelocity(fmu, AngV_FL, 1);
	
	//output torques to FrontLeft and FrontRight wheels
	TrqFL_out = -TrqFR;
	TrqFR_out = -TrqFL;
	brakeOut_out = BrakeOut;
	
	fmuTime = fmiPowertrain.getCurrentTime(fmu);
}

void UFmuFunction::SetFMUBP(FString fmuName, FString instanceName, float step_size)
{
	UE_LOG(LogTemp, Warning, TEXT("The FMU is being initialized"));
	fmu = fmiPowertrain.initialize_powertrain(fmuName, instanceName, step_size);
}

void UFmuFunction::StartInThread()
{
	fmiPowertrain.start_powertrain(fmu, true);
}


void UFmuFunction::DestroyFMU()
{
	fmiPowertrain.deinitialize_powertrain(fmu);
}

